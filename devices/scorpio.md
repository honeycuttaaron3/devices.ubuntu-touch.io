---
codename: 'scorpio'
name: 'Xioami Mi Note 2'
comment: 'wip'
icon: 'phone'
noinstall: true
maturity: .05
---

A community port has been started [here](https://forums.ubports.com/topic/3947/xiaomi-mi-note-2-scorpio).

### Working

 - Audio (partially, headphones don't work)
 - Calling
 - SMS
 - Wi-Fi
 - Wi-Fi Hotspot
 - Vibration
 - Orientation sensor
 - Proximity Sensor

### Not working

 - Bluetooth
 - Camera
 - Flashlight
 - GPS
