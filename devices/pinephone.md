---
codename: 'pinephone'
name: 'Pinephone'
comment: 'experimental'
icon: 'phone'
noinstall: true
maturity: .75
---

The [Pinephone](https://www.pine64.org/pinephone/) is an affordable ARM-based linux phone. An [experimental Ubuntu Touch image](https://ci.ubports.com/job/rootfs/job/rootfs-pinephone/) is available. You can find [installation instructions on GitLab](https://gitlab.com/ubports/community-ports/pinephone).
